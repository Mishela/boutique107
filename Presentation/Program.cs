﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation
{
    class Program
    {
        static void Main(string[] args)
        {
            CatalogueBU bu = new CatalogueBU();

            Console.Write("Nom du rayon : ");
            string nomRayon = Console.ReadLine();

            RayonEntity nouveauRayon = new RayonEntity()
            {
                Libelle = nomRayon
            };

            try
            {
                bu.AjouterRayon(nouveauRayon);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            foreach (RayonEntity r in bu.GetListeRayons())
            {
                Console.WriteLine("- " + r.Id + " : " + r.Libelle);
            }

            Console.ReadLine();
        }
    }
}
