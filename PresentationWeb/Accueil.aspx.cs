﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class Accueil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                List<RayonEntity> rayons = bu.GetListeRayons();

                // DATABINDING :

                // nom du champ à utiliser pour l'affichage :
                ddlRayons.DataTextField = "Libelle";
                // nom du champ à utiliser pour la valeur choisie : 
                ddlRayons.DataValueField = "Id";
                ddlRayons.DataSource = rayons;
                ddlRayons.DataBind();


                bulRayons.DataTextField = "Libelle";
                bulRayons.DataTextFormatString = "Rayon {0}";
                bulRayons.DataSource = rayons;
                bulRayons.DataBind();

            }
        }

        protected void btnTestRayons_Click(object sender, EventArgs e)
        {
            // récupération de la valeur sélectionnée :
            lblIdRayon.Text = ddlRayons.SelectedValue;
        }
    }
}