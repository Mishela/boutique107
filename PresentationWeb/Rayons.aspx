﻿<%@ Page Language="C#"%>

<%@ Import Namespace="AFCEPF.AI107.Boutique.Business" %>
<%@ Import Namespace="AFCEPF.AI107.Boutique.Entities" %>

<%
    CatalogueBU bu = new CatalogueBU();
    String message = "";
    String couleurMsg = "";

    // gérer ajout du rayon :
    if (Request.HttpMethod == "POST")
    {
        // récupérer le rayon 
        string libelleRayon = Request["txtRayon"];

        // créer une entité Rayon
        RayonEntity rayon = new RayonEntity();
        rayon.Libelle = libelleRayon;

        // demander au business de l'ajouter
        try
        {
            bu.AjouterRayon(rayon);
            message = "Ajout OK";
            couleurMsg = "green";
        }
        catch(Exception exc)
        {
            message = exc.Message;
            couleurMsg = "red";
        }
    }

    // récupérer liste rayons :
    List<RayonEntity> rayons = bu.GetListeRayons();

%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
   
    <form method="post" action="Rayons.aspx">
        <label for="txtRayon">Libelle : </label>
        <input type="text" name="txtRayon" id="txtRayon" placeholder="ex : habillement..."/>
        <input type="submit" value="Ajouter" id="btnAjout" name="btnAjout" />
        <label style="color:<%=couleurMsg%>;"><%=message %></label>
    <hr />
    <ul>
        <%foreach (RayonEntity r in rayons)
           { %>
             <li><%=r.Libelle %> (<%=r.Id %>)</li>
        <% } %>
    </ul>

</body>
</html>
