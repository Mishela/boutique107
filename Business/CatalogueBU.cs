﻿
using AFCEPF.AI107.Boutique.DataAccess;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Business
{
    public class CatalogueBU
    {
        // Pensez à mettre les regles de gestion ici !
        public List<RayonEntity> GetListeRayons()
        {
            RayonDAO dao = new RayonDAO();
            return dao.GetAll();
        }

        public void AjouterRayon(RayonEntity nouveauRayon)
        {
            bool trouve = false;
            RayonDAO dao = new RayonDAO();
            // vérifier que le libellé est unique :
            foreach (RayonEntity r in dao.GetAll())
            {
                if (r.Libelle == nouveauRayon.Libelle)
                {
                    trouve = true;
                }
            }

            if (!trouve)
            {
                // inserer un rayon
                dao.Insert(nouveauRayon);
            }
            else
            {
                throw new Exception("ERREUR : Rayon déjà existant");
            }
        }
    }
}
